package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// PLAYER indicate the binary of video player to be used
const PLAYER = "mpv"

// edit will clear the given file. Then launch editor and return the
// Cmd struct for editing
func edit(ctx context.Context, filename string) (*exec.Cmd, error) {
	// Clear file content
	err := ioutil.WriteFile(filename, []byte(""), 0644)
	if err != nil {
		return nil, err
	}

	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "vim"
	}

	cmd := exec.CommandContext(ctx, editor, filename)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Start()

	if err != nil {
		return nil, err
	}

	return cmd, nil
}

func genConfig(filename string) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	editCmd, err := edit(ctx, "config")

	if err != nil {
		return err
	}

	playCmd := exec.CommandContext(ctx, PLAYER, filename)
	playCmd.Stdin = nil
	playCmd.Stdout = nil
	playCmd.Stderr = nil

	if err := playCmd.Start(); err != nil {
		return err
	}

	if err := editCmd.Wait(); err != nil {
		return err
	}

	buf, err := ioutil.ReadFile("config")

	if str := string(buf); strings.Trim(str, " ") == "" {
		return errors.New("empty config. Abort")
	}

	return nil
}

func execSplit(filename string) error {
	cmd := exec.Command("ffsplit", filename)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func extractFilename(path string) string {
	_, file := filepath.Split(path)
	ext := filepath.Ext(file)

	return strings.TrimSuffix(file, ext)
}

func validateRes(filename string) error {
	prefix := extractFilename(filename)

	glob := "export/" + prefix + "*"
	files, err := filepath.Glob(glob)

	if err != nil {
		return err
	}

	cmd := exec.Command(PLAYER, files...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}

func removeSrc(filename string) error {
	fmt.Printf("Would you like to delete file %s? [Y/n] ", filename)

	reader := bufio.NewReader(os.Stdin)
	line, err := reader.ReadString('\n')

	if err != nil {
		return err
	}

	line = strings.TrimSuffix(line, "\n")

	if strings.EqualFold(line, "Y") || strings.Trim(line, " ") == "" {
		fmt.Println("Deleting file")

		if err := os.Remove(filename); err != nil {
			return err
		}

		fmt.Println("File deleted")
	}

	return nil
}

func run() error {
	if len(os.Args) < 2 {
		return errors.New("Expected to provide a video filename as argument")
	}

	filename := os.Args[1]

	if _, err := os.Stat(filename); err != nil && os.IsNotExist(err) {
		return errors.New("Specified filename does not exist")
	}

	err := genConfig(filename)
	if err != nil {
		return err
	}

	err = execSplit(filename)
	if err != nil {
		return err
	}

	err = validateRes(filename)
	if err != nil {
		return err
	}

	err = removeSrc(filename)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	err := run()
	if err != nil {
		log.Fatalf("Failed to process: %v\n", err)
	}
}
