import asyncio
from functools import wraps
from glob import iglob
from itertools import chain
import json
import os
from pathlib import Path
from typing import Union

from PIL import Image, ImageOps
from aiogram import Bot
from anyio import create_task_group, open_file, run, sleep
import httpx
from loguru import logger
from signalstickers_client import StickersClient
from signalstickers_client.models import LocalStickerPack, Sticker
import typer

from . import tg
from . import signal

app = typer.Typer()


def coro(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        # anyio's run doesn't support kwargs. So use asyncio here
        return asyncio.run(f(*args, **kwargs))

    return wrapper


@app.command()
@coro
async def download_tg(
    url: str = typer.Option(
        ...,
        help="URL of the sticker pack. For example, https://t.me/addstickers/nekostickerpack233",
    ),
    output: Path = typer.Option(
        "./output",
        "--output",
        "-o",
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        help="Output path for writing downloaded sticker and metadata",
    ),
):
    """Download a sticker pack from telegram."""
    sticker_name = tg.extract_sticker_url(url)

    token = tg.get_token()
    bot = Bot(token)

    logger.info("Downloading metadata for sticker", sticker_name=sticker_name)

    sticker_set = await bot.get_sticker_set(sticker_name)

    dir_prefix = output / sticker_set.name
    dir_prefix.mkdir(0o755, parents=True, exist_ok=True)

    logger.info("Downloading sticker images", title=sticker_set.title, to=dir_prefix)

    async with httpx.AsyncClient(
        http2=True, limits=httpx.Limits(max_connections=20)
    ) as client:
        tasks = [
            tg.download_file(client, bot, sticker.file_id, dir_prefix)
            for sticker in sticker_set.stickers
        ]

        res: list[Union[Path, Exception]] = await asyncio.gather(
            *tasks, return_exceptions=True
        )

    failed = [r for r in res if isinstance(r, Exception)]
    if len(failed) > 0:
        logger.warning("Failed to download some stickers")

    # Save metadata
    logger.info("Writing sticker metadata")
    meta = tg.StickerSetMeta.from_sticker(sticker_set, res)
    js = meta.json()
    async with await open_file(dir_prefix / "meta.json", "w") as file:
        await file.write(js)

    await bot.close()

    logger.info("Download completed")


@app.command()
def convert(
    globs: list[str] = typer.Argument(
        ...,
        help="List of globs to images for resizing",
    )
):
    """Convert stickers size specified by given globs.

    The result image will be override inplace and ensure to have 512x512 pixel.
    """
    glob_iter = chain.from_iterable(iglob(g) for g in globs)
    with typer.progressbar(glob_iter, label="Image convertion") as progress:
        for filename in progress:
            im = Image.open(filename)
            new_im = ImageOps.pad(im, (512, 512))
            new_im.save(filename)


@app.command()
@coro
async def upload_signal(
    meta_filepath: Path = typer.Option(
        ...,
        "--meta",
        "-m",
        file_okay=True,
        dir_okay=False,
        readable=True,
        resolve_path=True,
        help="Path to the meta.json file",
    )
):
    """Create and upload a signal sticker pack from given metadata.

    The meta.json file can be created by download_tg command.

    Run convert command on the images to ensure they comply with signal's
    requirement on sticker image.
    """

    async with await open_file(meta_filepath) as file:
        content = await file.read()

    data = json.loads(content)
    meta = tg.StickerSetMeta(**data)

    path_prefix = meta_filepath.parent

    logger.info(
        "Loaded meta from {} with {} stickers", meta_filepath, len(meta.stickers)
    )

    username = signal.get_username()
    password = signal.get_password()

    pack = LocalStickerPack()

    pack.title = meta.title
    pack.author = "Telegram"

    for sticker in meta.stickers:
        stick = Sticker()
        stick.id = pack.nb_stickers
        stick.emoji = sticker.emoji

        async with await open_file(path_prefix / sticker.filename, "rb") as file:
            stick.image_data = await file.read()

        pack._addsticker(stick)

    logger.info("Created local sticker pack. Uploading it to signal")

    async with StickersClient(username, password) as client:
        # Upload the pack
        pack_id, pack_key = await client.upload_pack(pack)

    url = f"https://signal.art/addstickers/#pack_id={pack_id}&pack_key={pack_key}"
    logger.info("Pack uploaded. URL = {}", url)
