from __future__ import annotations
import os
from pathlib import Path
from typing import Union, cast
from urllib.parse import urlparse

from aiogram.bot.bot import Bot
from aiogram.types.sticker import Sticker
from aiogram.types.sticker_set import StickerSet
from anyio import open_file
from httpx import AsyncClient
from loguru import logger
from pydantic import BaseModel
import typer


class StickerMeta(BaseModel):
    """Model for a sticker."""

    filename: str
    is_animated: bool
    emoji: str

    @classmethod
    def from_sticker(cls, sticker: Sticker, i: int) -> StickerMeta:
        """Create metadata from given sticker.

        :param sticker: Sticker from telegram bot model
        :param i: Index of the sticker position
        """
        return cls(
            filename=str(i), is_animated=sticker.is_animated, emoji=sticker.emoji
        )


class StickerSetMeta(BaseModel):
    """Model for a sticker set."""

    name: str
    title: str
    stickers: list[StickerMeta]

    @classmethod
    def from_sticker(
        cls, sticker_set: StickerSet, filenames: list[Union[Exception, Path]]
    ) -> StickerSetMeta:
        """Create metadata from given stickerset.

        :param sticker_set: The sticker set data
        :param filenames: Filename of the downloaded files
        """
        stickers = [
            StickerMeta(
                filename=cast(Path, filenames[i]).name,
                is_animated=s.is_animated,
                emoji=s.emoji,
            )
            for i, s in enumerate(sticker_set.stickers)
            if not isinstance(filenames[i], Exception)
        ]
        return cls(name=sticker_set.name, title=sticker_set.title, stickers=stickers)


def get_token() -> str:
    """Get telegram bot token.

    By default we will look for environment variable ``TG_BOT_TOKEN``. If that variable
    is not defined, we will post a prompt to the user.

    :returns: Telegram bot token supplied by the user
    """
    ENV_NAME = "TG_BOT_TOKEN"

    env = os.getenv(ENV_NAME)
    if env is not None and env != "":
        return env

    return typer.prompt("Input telegram bot API token", type=str)


def extract_sticker_url(url: str) -> str:
    """Extract sticker name from given url.

    The sticker url must be starting with "https://t.me/addstickers/".

    :param url: URL to be parsed
    :returns: Sticker name
    :raises ValueError: Given url is not in correct form
    """
    url = url.strip()
    prefix = "https://t.me/addstickers/"

    if not url.startswith(prefix):
        raise ValueError(f"Sticker URL must starts with {prefix}")

    name = url.removeprefix(prefix)
    if "/" in name:
        raise ValueError("Incorrect sticker URL. Tailing slash is not allowed")

    return name


async def download_file(
    client: AsyncClient, bot: Bot, file_id: str, dest: Path
) -> Path:
    """Download a file from telegram.

    If the destination already have a file exist, the file will be overwritten without
    any warnings.

    :param client: HTTPx client for doing request
    :param bot: Bot instance for requesting
    :param file_id: File ID of interest
    :param dest: Destination directory
    :returns: Path of the downloaded file
    """
    logger.debug("Downloading file", file_id=file_id, dest=dest)

    file = await bot.get_file(file_id)
    url = bot.get_file_url(file.file_path)

    # Get the file suffix from the URL
    parsed = urlparse(url)
    suffix = Path(parsed.path).suffix

    filepath = dest / f"{file.file_unique_id}{suffix}"

    if filepath.is_file():
        logger.info("File already exist", filepath=filepath)
        return filepath

    try:
        async with client.stream("GET", url) as resp, await open_file(
            filepath, "bw"
        ) as file:
            async for chunk in resp.aiter_bytes():
                await file.write(chunk)
    except Exception:
        logger.exception("Failed to download file", dest=dest, file_id=file_id)

        if dest.is_file():
            logger.debug("File was created. Deleting unfinished file.")
            os.remove(dest)
        raise

    logger.debug("Download file completed")

    return filepath
