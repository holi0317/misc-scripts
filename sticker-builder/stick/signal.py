import os
from typing import Tuple

import typer


def _prompt(env: str, question: str) -> str:
    """Try to get from environment variable or send a prompt to user.

    :param env: Environment variable to look for
    :param question: Question for asking user when the environment variable is
        not defined
    :returns: Value sourced from environment variable or from user's input
    """
    e = os.getenv(env)
    if e is not None and e != "":
        return e

    return typer.prompt(question, type=str)


def get_username() -> str:
    """Get signal username.

    Will try to source from environment ``SIGNAL_USERNAME`` or show a prompt
    to ask from user.
    """
    return _prompt("SIGNAL_USERNAME", "Input signal username")


def get_password() -> str:
    """Get signal password.

    Will try to source from environment ``SIGNAL_PASSWORD`` or show a prompt
    to ask from user.
    """
    return _prompt("SIGNAL_PASSWORD", "Input signal password")
